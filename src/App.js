import avatar from "./assets/images/avatar.jpg";

function App() {
  return (
    <div style={{width: "800px", margin: "100px auto", textAlign: "center", border: "1px solid #ddd"}}>
      <div>
        <img style={{borderRadius: "50%", marginTop: "-50px"}} src={avatar} width={100} alt="avatar"/>
      </div>
      <div>
        <p style={{margin: "20px auto", fontSize: "18px"}}>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      </div>
      <div>
        <p style={{fontSize: "15px", marginBottom: "30px"}}><b>Tammy Stevens</b> * Front End Developer</p>
      </div>
    </div>
  );
}

export default App;
